# Safe Filename function - Thanks https://gist.github.com/st0le/b6cb31272e1e03a7e1d479b2686d3e3e#file-playstvdownloader-ps1
# Modified to URL-Escape the string (slashes/questionmarks/etc) and enable dynamic file extension (images)
function Safe-Title {
    param (
        $Description,
        $EpochMilliseconds,
        $FileExtension
    )
    $CreatedDate = (Get-Date "1970-01-01 00:00:00.000Z") + ([TimeSpan]::FromSeconds($EpochMilliseconds / 1000))
    $Title = $Description -replace "[^\w]+", "_"
    $Title = [uri]::EscapeDataString($Title.Substring(0, [System.Math]::Min(30, $Title.Length)))
    return (get-date $CreatedDate -uformat "%Y-%m-%d") + " - " + $Title + $FileExtension
}

# Metadata extraction from Plays.tv API
function Get-PlaysTVMetadata {

    if (Test-Path "./uploaded.json" -PathType Leaf) {
        Write-Host "Found manual export - Importing!"
        $data = Get-Content "./uploaded.json" -Raw |ConvertFrom-JSON
    } else {
    
        # Create IE window for login
        Write-Host "Launching IE ..."
        $loginwindow = new-object -ComObject "InternetExplorer.Application"
        $loginwindow.silent = $true
        $loginwindow.navigate("https://plays.tv/profile")
        $loginwindow.visible = $true
        while($loginwindow.Busy){sleep 1}

        # Check if logged in
        Write-Host "Checking Login... "
        $loggedin = $false
        while (!$loggedin) {
            # The data-conf attribute holds the user details
            $username = ($loginwindow.document.documentElement.getAttribute('data-conf') |ConvertFrom-Json).login_user.urlname
            $userid = ($loginwindow.document.documentElement.getAttribute('data-conf') |ConvertFrom-Json).login_user.id
            $feedurl = "https://plays.tv/playsapi/feedsys/v1/userfeed/" + $userid + "/uploaded?limit=9999&filter="

            # The "guest" user when you're not logged in is "raptr_master"
            if ($username -eq "raptr_master") {
                Start-Sleep 1
            } else {
                $loggedin = $true
            }
        }

        Write-Host "**** Logged in as $username ($userid)"

        # Launch the background process to get the feed
        Write-Host "Launching background process ..."
        $ie = new-object -ComObject "InternetExplorer.Application"
        $ie.silent = $true

        # Load the 404 error page - smallest non-autoplay page I could find
        $ie.navigate("https://plays.tv/oops")
        while($ie.Busy){sleep 1}

        # NOTE
        # You can't directly access the feed URL in IE
        # Because it's a JSON file, IE will prompt to download
        # This is a genious workaround! :)

        # Create a div to hold the feed JSON data
        $div = $ie.document.IHTMLDocument2_createElement("div")
        $div.id = "feed"
        $ie.document.body.appendChild($div)
        Clear-Host

        # Create a script to XHR the feed into the div
        $script = $ie.document.IHTMLDocument2_createElement("script")
        $script.text = "var xhr=new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if(xhr.readyState !== 4) return;
                if(xhr.status >= 200 && xhr.status < 300){
                    document.getElementById('feed').innerHTML = xhr.responseText;
                } else {
                    $div.innerHTML = ''
                }
            };
            xhr.onerror = function() {
                $div.innerHTML = ''
            }
            xhr.open('GET','" + $feedurl + "');
            xhr.send();"

        # Attach the script to the DOM (and execute it)
        $ie.document.body.appendChild($script)
        Clear-Host
        Write-Host "Waiting for the data ..."

        $i = 0; $abort = 120; $success = $false
        while($div.innerHTML -eq $null) {
            if (++$i -gt $abort) {
                Write-Host "ERROR! Feed didn't load in time ($($abort)s). Please report this error."
                $ie.Quit(); $loginwindow.Quit()
                Return
            } else {
                if (($i % 10) -eq 0) {
                    Write-Host "Still waiting up to $($abort - $i)s longer ..."
                }
                Start-Sleep 1
            }
        }
        Write-Host "Feed data loaded in $i seconds!"
        
        # Grab the feed data from the div
        $jsonfeed = $div.innerHTML
        $data = $jsonfeed |ConvertFrom-Json

        # Clean up IE
        Write-Host "Cleaning up IE ..."
        $ie.Quit()
        $loginwindow.Quit()
    }
    
    $items = $data.Items
    Write-Host "Feed data contained $($items.count) objects."

    # Split images out separately because they're smaller
    Write-Host "Sorting data ..."
    $images = @($items |Where-Object {$_.type -eq "image"})
    Write-Host "$($images.count) images found."
    $videos = @($items |Where-Object {$_.type -eq "video"})
    Write-Host "$($videos.count) videos found."

    # Cache this data, since it's unlikely to change and a pain to get
    Write-Host "Exporting data ..."
    $images |Export-Clixml "./Images.xml"
    $videos |Export-Clixml "./Videos.xml"

    Write-Host "Metadata Export Complete!"
}

# Bunches the images into a single BITS job
function Start-PlaysTVImageDownload {
    Write-Host "Loading image metadata ..."
    $images = @(Import-Clixml "./Images.xml")
    if ($images.Count -eq 0) {
        Write-Host "No images to download."
        Return
    }
    
    Import-Module BitsTransfer
    New-Item -ItemType Directory -Force -Path "./Output/" |Out-Null

    Write-Host "$($images.count) items loaded."

    Write-Host "Building file list ..."
    Foreach ($item in $images) {
        $file = Safe-Title $item.description $item.created $([System.IO.Path]::GetExtension($item.downloadurl))
        $path = ".\Output\"
        # Create the folder if it doesn't exist
        New-Item -ItemType Directory -Force -Path $path |Out-Null

        $files += @{
            Source = $item.downloadurl
            Destination = $path + $file
        }
    }
    Write-Host "Creating BITS task ..."
    $task = Start-BitsTransfer -Source $files.Source -Destination $files.Destination -TransferType Download -DisplayName "PlaysTV Downloader - Bulk Images" -Asynchronous
    $task.DisplayName
}

# Creates a BITS job for each video, tagged with the video ID
function Start-PlaysTVVideoDownload {
    Write-Host "Loading video metadata ..."
    $videos = @(Import-Clixml "./Videos.xml")
    if ($videos.Count -eq 0) {
        Write-Host "No videos to download."
        Return
    }
    
    Import-Module BitsTransfer
    New-Item -ItemType Directory -Force -Path "./Output/" |Out-Null
    Write-Host "$($videos.count) items loaded."

    Write-Host "Creating BITS tasks ..."
    $maxtasks = 50
    if ($videos.Count -lt $maxtasks) {
        Write-Host "Less than $maxtasks videos to download - speed mode!"
        Foreach ($item in $videos) {
            $file = Safe-Title $item.description $item.created $([System.IO.Path]::GetExtension($item.downloadurl))
            $path = ".\Output\$file"

            $task = Start-BitsTransfer -Source $item.downloadurl -Destination $path -TransferType Download -DisplayName "PlaysTV Downloader - Video $($item.feedId)" -Asynchronous
            $task.DisplayName
        }
    } else {
        Write-Host "More than $maxtasks videos to download - batching mode!"
        $chunksize = [math]::Ceiling($videos.Count / $maxtasks)
        $chunkcount = [math]::Ceiling($videos.Count / $chunksize)
        $chunks = @()
        Write-Host "Batching $chunksize videos in each of $chunkcount tasks ..."

        for($i=0; $i -le $maxtasks; $i++){
            $start = $i*$chunksize
            $end = (($i+1)*$chunksize)-1
            $chunks += ,@($videos[$start..$end])
        }

        $id = 0
        Foreach ($chunk in $chunks) {
            if ($chunk.Count -gt 0) {
                $source = @()
                $destination = @()
                $id++
                Foreach ($item in $chunk) {
                    $file = Safe-Title $item.description $item.created $([System.IO.Path]::GetExtension($item.downloadurl))
                    $path = ".\Output\$file"

                    $source += @($item.downloadurl)
                    $destination += @($path)
                }

                $task = Start-BitsTransfer -Source $source -Destination $destination -TransferType Download -DisplayName "PlaysTV Downloader - Video Chunk $id" -Asynchronous
                $task.DisplayName
            }
        }

    }
}

# This just loops over the BITS jobs and displays them
# Might be too slow if you have THOUSANDS of files, but we'll see
function Monitor-PlaysTVContentDownload {
    Write-Host "Monitoring: Loading task metadata ..."
    $bits = @(Get-BitsTransfer |Where-Object {$_.DisplayName -match "PlaysTV Downloader"})
    Write-Host "Loaded $($bits.count) task(s) ..."

    while($bits) {
        Foreach ($task in $bits) {
            if ($task.JobState -eq "Transferred") {
                Write-Host "Completed: $($task.DisplayName)"
                Complete-BitsTransfer -BitsJob $task
                $update = $true
            }   
        }
        $bits = @(Get-BitsTransfer |Where-Object {$_.DisplayName -match "PlaysTV Downloader"})
        if ($update) {Write-Host "Waiting on $($bits.count) task(s) ..."; $update = $false}
        Start-Sleep 1
    }
}

# Check if there's existing transfers, then just monitor them
if (@(Get-BitsTransfer |Where-Object {$_.DisplayName -match "PlaysTV Downloader"})) {
    Write-Host "Found BITS transfers in progress - Resuming previous run!"
    Monitor-PlaysTVContentDownload
    Write-Host "All done!"
    Return
}

# If there's metadata, use it so we don't have to fuck with IE automation
if ((Test-Path "./Images.xml" -PathType Leaf) -or (Test-Path "./Videos.xml" -PathType Leaf)) {
    Write-Host "Found exported metadata!"
    Write-Host "To overwrite this, delete both of the XML files in this folder."
} else {
    Get-PlaysTVMetadata
}

# BITS will overwrite files without warning, so this is needed
if (Test-Path "./Output") {
    Write-Host "Output folder already found - this may overwrite files!"
    Write-Host "Press ENTER to continue if you are ok with this risk,"
    Write-Host "   or close the window to abort."
    Read-Host |Out-Null
}

# Images first, since they're smaller
if (Test-Path "./Images.xml" -PathType Leaf) { Start-PlaysTVImageDownload }
if (Test-Path "./Videos.xml" -PathType Leaf) { Start-PlaysTVVideoDownload }
Monitor-PlaysTVContentDownload

Write-Host "All done!"
