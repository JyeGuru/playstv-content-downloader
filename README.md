# Plays.TV Content Downloader

## About

This is another Plays.TV Content Download Script, however unlike most of them, this one:
- Doesn't need anything else installed (on Windows 10 at least)
- Doesn't need admin rights
- Doesn't ask for your password ever - it uses an IE session
- Pulls all images and videos, even private ones!
- Uses BITS for transfers, so it keeps running in the background once it's initialised
- Can be copy/pasted into a Powershell console, or downloaded and run, whichever you prefer!

## Usage Instructions

This script can be most easily launched using `Invoke-WebRequest` and `Invoke-Expression`. Simply do the following:

1. Browse to the folder you want your files in
2. Shift-Right-Click on the folder
    - If you have "Open Powershell Window Here" use that
    - Otherwise, use "Open Command Window  Here" and then type `powershell` into that window
3. At the Powershell prompt, paste this line:
```
Invoke-WebRequest "https://gitlab.com/JyeGuru/playstv-content-downloader/raw/master/Start-PlaysTVContentDownload.ps1" |Invoke-Expression
```

I've also made a shorter line, which you can easily type instead:
```
iwr "https://tinyurl.com/playstvdownloader" |iex
```

These will both do the same thing, running the script to download all your content into the selected folder.
If you have any issues with this, you can hit me up on Discord (`JyeGuru#1234` or via the PlaysTV server).

## Troubleshooting

If the script fails to download your feed, and the manual download never prompts in the IE window, you can download the file manually.
Simply go to the URL below (substitute your own username) and save the page as `uploaded.json` in the script's folder
```
https://plays.tv/playsapi/feedsys/v1/userfeed/YOURUSERNAME/uploaded?limit=9999&filter=
```
Then, when you run it again it will detect this file and use it instead of going to get the online version.
